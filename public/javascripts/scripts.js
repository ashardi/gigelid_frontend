(function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation-register');
    var forms_editprofile = document.getElementsByClassName('needs-validation-editprofile');
    var forms_changepwd = document.getElementsByClassName('needs-validation-changepwd');

    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        var pwd1 = document.getElementById('password1').value;
        var pwd2 = document.getElementById('password2').value;

        if (/^([a-zA-Z0-9]+)$/.test(pwd1) && /\d/.test(pwd1) && /[A-Z]/i.test(pwd1)) {
          if(pwd1.length < 8) {
            alert("Password minimum 8 characters!");
            event.preventDefault();
            event.stopPropagation();
          } else {
            if(pwd1 !== pwd2) {
              alert("Password and Confirm Password not matched!");
              event.preventDefault();
              event.stopPropagation();
            }
          }
        } else {
          alert("Password must contains letters and numbers");
          event.preventDefault();
          event.stopPropagation();
        }

        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
    
    var validation_editprofile = Array.prototype.filter.call(forms_editprofile, function(forms_editprofile) {
      forms_editprofile.addEventListener('submit', function(event) {
        if (forms_editprofile.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        forms_editprofile.classList.add('was-validated');
      }, false);
    });
    
    var validation_changepwd = Array.prototype.filter.call(forms_changepwd, function(forms_changepwd) {
      forms_changepwd.addEventListener('submit', function(event) {
        var pwd1 = document.getElementById('password1').value;
        var pwd2 = document.getElementById('password2').value;

        if (/^([a-zA-Z0-9]+)$/.test(pwd1) && /\d/.test(pwd1) && /[A-Z]/i.test(pwd1)) {
          if(pwd1.length < 8) {
            alert("Password minimum 8 characters!");
            event.preventDefault();
            event.stopPropagation();
          } else {
            if(pwd1 !== pwd2) {
              alert("Password and Confirm Password not matched!");
              event.preventDefault();
              event.stopPropagation();
            }
          }
        } else {
          alert("Password must contains letters and numbers");
          event.preventDefault();
          event.stopPropagation();
        }
        
        forms_changepwd.classList.add('was-validated');
      }, false);
    });
  }, false);
})();