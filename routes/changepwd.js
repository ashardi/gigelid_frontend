var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function(req, res, next) {
  if(typeof(req.session._id) == 'undefined') res.redirect('/?err=Session expired');
  var info;

  res.render('changepwd', { 
    title: 'Change Password',
    active_dashboard: false,
    active_editprofile: false,
    active_changepwd: true,
    info: req.query.info
  });
});

router.post('/', function(req, res, next) {
  var data = {};
  var id = req.session._id;

  axios.post(gbBaseurl+'/user/changepwd/'+id, {
    'oldpassword': req.body.oldpassword,
    'password1': req.body.password1,
    'password2': req.body.password2
  })
  .then(response => {
    if(response.data.status == 'ok') {
      res.redirect('/changepwd?info=Data updated');
    } else {
      res.redirect('/changepwd?info='+response.data.result);
    }
  })
  .catch(error => {
    console.log(error);
  });
});

module.exports = router;
