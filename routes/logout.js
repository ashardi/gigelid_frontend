var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function(req, res, next) {
  req.session.destroy(function(err){
    if(err){
      console.log(err);
    }
  });
  res.redirect('/');
});

module.exports = router;
