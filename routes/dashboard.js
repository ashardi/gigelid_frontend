var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function(req, res, next) {
  if(typeof(req.session._id) == 'undefined') res.redirect('/?err=Session expired');
  var data = {};

  // Get users list
  axios.get(gbBaseurl+'/users')
  .then(response => {
    data = response.data;
    res.render('dashboard', { 
      title: 'Dashboard',
      active_dashboard: true,
      active_editprofile: false,
      active_changepwd: false,
      data: data.result
    });
  })
  .catch(error => {
    console.log(error);
  });
});

module.exports = router;
