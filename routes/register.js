var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function(req, res, next) {
  var info;

  res.render('register', { 
    title: 'Registration',
    info: req.query.info
  });
});

router.post('/', function(req, res, next) {
  var data = {};

  axios.post(gbBaseurl+'/users', {
    'name': req.body.name,
    'email': req.body.email,
    'password': req.body.password1,
    'phone': req.body.phone
  })
  .then(response => {
    if(response.data.status == 'ok') {
      res.redirect('/?err=Please login');
    } else {
      res.redirect('/register?info='+response.data.result);
    }
  })
  .catch(error => {
    console.log(error);
  });
});

module.exports = router;