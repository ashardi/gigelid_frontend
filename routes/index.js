var express = require('express');
var router = express.Router();
const axios = require('axios');

/* GET home page. */
router.get('/', function(req, res, next) {
  var err;
  if(req.query) {
    err = req.query.err;
  }
  res.render('index', { title: 'Gigel.ID Test - Anton Ashardi', err: err });
});

router.post('/', function(req, res, next) {
  var email = req.body.email;
  var password = req.body.password;
  axios.post(gbBaseurl+'/auth', {
    'email': email, 'password': password
  })
  .then(response => {
    if(response.data.status == 'ok') {
      req.session._id = response.data.result._id;
      res.redirect('/dashboard');
    } else {
      res.redirect('/?err=Email or password is not matched.');
    }
  })
  .catch(error => {
    console.log(error);
  });
});


module.exports = router;
