var express = require('express');
var router = express.Router();
const axios = require('axios');

router.get('/', function(req, res, next) {
  if(typeof(req.session._id) == 'undefined') res.redirect('/?err=Session expired');

  var data = {};
  var info;

  axios.get(gbBaseurl+'/user/_id/'+req.session._id)
  .then(response => {
    res.render('editprofile', { 
      title: 'Edit My Profile',
      active_dashboard: false,
      active_editprofile: true,
      active_changepwd: false,
      item: response.data.result,
      info: req.query.info
    });
  })
  .catch(error => {
    console.log(error);
  });
});

router.post('/', function(req, res, next) {
  var data = {};
  axios.patch(gbBaseurl+'/user/'+req.session._id, {
    'name': req.body.name,
    'phone': req.body.phone,
  })
  .then(response => {
    if(response.data.status == 'ok') {
      res.redirect('/editprofile?info=Data updated');
    } else {
      res.redirect('/editprofile?info='+response.data.result);
    }
  })
  .catch(error => {
    console.log(error);
  });
});

module.exports = router;
