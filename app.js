var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var registerRouter = require('./routes/register');
var dashboardRouter = require('./routes/dashboard');
var editprofileRouter = require('./routes/editprofile');
var changepwdRouter = require('./routes/changepwd');
var loginRouter = require('./routes/index');
var logoutRouter = require('./routes/logout');

var app = express();

app.use(cookieParser());
app.use(session({ secret: 'antonganteng', resave: true, saveUninitialized: true, cookie: { maxAge: 999999999 }}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/register', registerRouter);
app.use('/dashboard', dashboardRouter);
app.use('/editprofile', editprofileRouter);
app.use('/changepwd', changepwdRouter);
app.use('/login', loginRouter);
app.use('/logout', logoutRouter);

global.gbBaseurl = 'http://127.0.0.1:5000';

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
